#!/bin/sh

allsize=(16 22 24 32 48 64 72 96 128 150 192 256 384 512 1024)

for x in ${allsize[@]}
do
	echo "$x"
	/usr/bin/inkscape tblock.svg --export-filename=tblock"$x".png -w "$x" -h "$x"
done