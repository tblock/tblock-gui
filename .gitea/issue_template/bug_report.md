---

name: "Bug Report"
about: "There is a bug in the software!"
title: "A clear and short title"
ref: "main"
labels:
- "Kind: Bug"
- "Status: Stale"
- "Priority: High"

---

### Before opening:
<!--
Before opening, please ensure that this issue is not duplicated.
Also, please remember that the more information you provide about the bug, the
faster we will be able to work on a fix.
-->

- [ ] This issue is not duplicated
- [ ] I am running [the latest stable version of TBlock GUI](https://codeberg.org/tblock/tblock-gui/releases/latest)
- [ ] I am running [the latest stable version of TBlock](https://codeberg.org/tblock/tblock/releases/latest)

### Bug description
<!--
Please write a clear and concise description
-->

### Steps to reproduce

- This issue is repoducible: `yes/no`
<!-- 
If it is reproducible, enter the steps to reproduce it below
-->

1. Do something
2. Do something else
3. See error


### Error code

- This issue results in TBlock GUI to freeze: `yes/no`
- This issue results in TBlock GUI to crash: `yes/no`
<!-- 
If it results in a crash, please enter the output of your terminal emulator below (you can view it by launching "tblockg" from your terminal emulator).
-->

```

```

### Possible solutions or causes
<!--
If you are a developer, you can write your ideas about what causes this bug or how to fix it in this section. Otherwise, leave it blank or delete it.
-->


### System information
<!--
This section is not mandatory, but they can be really helpful in some cases.
If, however, you don't wish to disclose such information, you are free to delete this section.
-->

I am running TBlock GUI on:
- [ ] GNU/Linux, Linux: `distribution`
- [ ] macOS: `version`
- [ ] Microsoft Windows: `version`
- [ ] *BSD
- [ ] Other: `name`, `version`

I installed TBlock GUI on my machine with:
- [ ] My distribution's official package repository
- [ ] AUR/Chaotic-AUR
- [ ] Fedora COPR
- [ ] Ubuntu PPA
- [ ] Debian package
- [ ] Homebrew
- [ ] Scoop
- [ ] Windows EXE installer
- [ ] Python pip
- [ ] Manually (with `make`) from the latest tag
- [ ] Manually (with `make`) from the main branch

### Logs
<!-- 
If you want, you can attach TBlock's log file for more information.
This is not required, but it can be useful for some bugs. Feel free to delete this section if you don't want to provide your log file.

Under UNIX-like systems, you can find the log file under:
-> /var/log/tblock-gui.log

