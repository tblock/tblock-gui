# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-18 00:16+0100\n"
"PO-Revision-Date: 2022-12-02 05:02+0000\n"
"Last-Translator: Rebecca Södergren <rebecca.sodergren@protonmail.com>\n"
"Language-Team: Swedish <https://translate.codeberg.org/projects/tblock/"
"tblock-gui/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.14.2\n"

#: tblock_gui/const.py:39
msgid "Auto-detect"
msgstr "Detektera automatiskt"

#: tblock_gui/const.py:40
msgid "ABP/AdGuard/uBO format"
msgstr "ABP/AdGuard/uBO format"

#: tblock_gui/const.py:41
msgid "dnsmasq configuration"
msgstr "dnsmasq konfiguration"

#: tblock_gui/const.py:42
msgid "Hosts file format"
msgstr "Hosts filformat"

#: tblock_gui/const.py:43
msgid "Plain text domain list"
msgstr "Klartext domänlista"

#: tblock_gui/const.py:44
msgid "TBlock format v2"
msgstr "TBlock format v2"

#: tblock_gui/const.py:45
msgid "TBlock format v1 (legacy)"
msgstr "TBlock format v1 (arv)"

#: tblock_gui/const.py:83
msgid "TBlock is currently not supported on your operating system"
msgstr "TBlock stöds för tillfället inte av ditt operativsystem"

#: tblock_gui/custom_add.py:123
msgid ""
"Choose this if you are not sure. Note that it may take more time than usual."
msgstr ""
"Välj detta om du inte är säker. Notera att det kan ta längre tid än vanligt."

#: tblock_gui/custom_add.py:129
msgid ""
"The default format used in most browser add-ons. TBlock supports it "
"partially only."
msgstr ""
"Standardformatet som används i de flesta webbläsartillägg. TBlock stödjer "
"det endast delvis."

#: tblock_gui/custom_add.py:135
msgid ""
"Import your dnsmasq.conf rules into TBlock. This format is partially "
"supported."
msgstr ""
"Importera din dnsmasq.conf regler in till TBlock. Detta format stöds delvis."

#: tblock_gui/custom_add.py:141
msgid ""
"Import your /etc/hosts rules into TBlock. This format is fully supported."
msgstr ""
"Importera din /etc/hosts regler in till TBlock. Detta format stöds fullt ut."

#: tblock_gui/custom_add.py:147
msgid "A simple list of domains to block. TBlock has native support for it."
msgstr ""
"En enkel lista av dina domäner att blockera. TBlock har gediget stöd för "
"detta."

#: tblock_gui/custom_add.py:152
#, fuzzy
msgid "The official format that is fully supported by TBlock."
msgstr "Det officiella formatet som är 100% kompatibelt med TBlock."

#: tblock_gui/custom_add.py:157
msgid ""
"This format is deprecated and support will be removed in future versions."
msgstr ""
"Detta format är utfasat och stödet kommer tas bort i en framtida version."

#: tblock_gui/custom_add.py:202
msgid "Please choose a file"
msgstr "Välj en fil"

#: tblock_gui/custom_add.py:206 tblock_gui/rules.py:107 data/ui/tblock.ui:925
msgid "Select"
msgstr "Välj"

#: tblock_gui/custom_add.py:210
msgid "Text files"
msgstr "Textfiler"

#: tblock_gui/custom_add.py:213
msgid "Any files"
msgstr "Alla filer"

#: tblock_gui/error.py:73
msgid "This operation requires admin privileges."
msgstr "Denna funktion kräver administratörsprivilegier."

#: tblock_gui/error.py:80
msgid ""
"TBlock executable was not found.\n"
"Please install it and then retry."
msgstr ""

#: tblock_gui/error.py:88
msgid ""
"An unknown error occurred.\n"
"Code: {}"
msgstr ""

#: tblock_gui/error.py:95
msgid "This feature is not yet supported."
msgstr ""

#: tblock_gui/error.py:103
msgid ""
"The database is locked. Please try again later.\n"
"If this error persists, you can unlock the database in the navigation menu "
"under TBlock > Troubleshooting."
msgstr ""
"Databasen är låst. Försök igen senare.\n"
"Om felet kvarstår, du kan låsa upp databasen i navigationsmenyn under TBlock "
"> Felsök."

#: tblock_gui/error.py:113
msgid "Please provide a valid URL/local file."
msgstr "Vänligen förse en giltig URL/lokal fil."

#: tblock_gui/error.py:119
msgid "File must be owned by root."
msgstr ""

#: tblock_gui/error.py:126
msgid "Please specify a title to give to your custom filter list."
msgstr "Vänligen ange en titel till din anpassningsfilterlista."

#: tblock_gui/error.py:133
msgid "Source already exists."
msgstr "Källan existerar redan."

#: tblock_gui/error.py:139
msgid "The database is already unlocked."
msgstr "Databasen är redan låst."

#: tblock_gui/filter_lists.py:141
msgid "Active"
msgstr ""

#: tblock_gui/filter_lists.py:144
msgid "ID"
msgstr "ID"

#: tblock_gui/filter_lists.py:147
msgid "Title"
msgstr "Titel"

#: tblock_gui/filter_lists.py:192
msgid "This is a custom filter list that was manually added by the user."
msgstr ""

#: tblock_gui/filter_lists.py:201
msgid "No filter list selected"
msgstr ""

#: tblock_gui/filter_lists.py:203
msgid "Click on a filter list from the table to view its details."
msgstr ""

#: tblock_gui/intro.py:120
msgid "A profile that will let you configure everything yourself."
msgstr "En profil som kommer låta dig konfigurera allt själv."

#: tblock_gui/intro.py:123
msgid "This profile is light, but some ads and trackers won't be blocked."
msgstr ""
"Denna profil är lätt, men vissa annonser och trackers kommer inte att "
"blockas."

#: tblock_gui/intro.py:127
msgid "This efficient profile is the perfect solution for regular users."
msgstr ""
"Denna effektiva profil är den perfekta lösningen för den genomsnittlige "
"användaren."

#: tblock_gui/intro.py:130
msgid "A powerful profile that may however break some web pages."
msgstr "En kraftfull profil som kan ha sönder vissa webbsidor."

#: tblock_gui/job.py:132 data/ui/tblock.ui:833 data/ui/tblock.ui:1179
#: data/ui/tblock.ui:2316 data/ui/tblock.ui:2456
msgid "Nothing to do."
msgstr ""

#: tblock_gui/job.py:147
msgid "You can now press Close to close this dialog."
msgstr ""

#: tblock_gui/job.py:150
msgid "Operation finished successfully."
msgstr ""

#: tblock_gui/job.py:154
msgid "Operation finished with {} error(s)."
msgstr ""

#: tblock_gui/job.py:228 tblock_gui/settings.py:135
msgid "Failed to change GUI settings."
msgstr ""

#: tblock_gui/job.py:316
msgid "Creating database"
msgstr ""

#: tblock_gui/job.py:318
msgid "Failed to create database"
msgstr ""

#: tblock_gui/job.py:319
msgid "Syncing filter list repository index"
msgstr ""

#: tblock_gui/job.py:321
#, fuzzy
msgid "Failed to sync filter list repository index."
msgstr "Kunde inte hämta filterlistans förvarsindex."

#: tblock_gui/job.py:322
msgid "Downloading icon pack"
msgstr ""

#: tblock_gui/job.py:324
msgid "Failed to download icon pack."
msgstr ""

#: tblock_gui/job.py:327
msgid "Extracting icon pack"
msgstr ""

#: tblock_gui/job.py:329
msgid "Failed to extract icon pack."
msgstr ""

#: tblock_gui/job.py:335
msgid "Building hosts file"
msgstr ""

#: tblock_gui/job.py:337 tblock_gui/main.py:148
msgid "Failed to build hosts file."
msgstr ""

#: tblock_gui/job.py:344
msgid "Adding custom filter list: {}"
msgstr ""

#: tblock_gui/job.py:352
msgid "Failed to add custom filter list: {}."
msgstr ""

#: tblock_gui/job.py:363
msgid "Retrieving: {}"
msgstr ""

#: tblock_gui/job.py:365
msgid "Failed to download: {}"
msgstr ""

#: tblock_gui/job.py:375
#, fuzzy
msgid "Subscribing to: {}"
msgstr "Prenumeration"

#: tblock_gui/job.py:377
msgid "Failed to subscribe to: {}"
msgstr ""

#: tblock_gui/job.py:387
#, fuzzy
msgid "Unsubscribing from: {}"
msgstr "Prenumeration"

#: tblock_gui/job.py:389
msgid "Failed to unsubscribe from: {}"
msgstr ""

#: tblock_gui/job.py:398
msgid "Removing from cache: {}"
msgstr ""

#: tblock_gui/job.py:407
msgid "Updating: {}"
msgstr ""

#: tblock_gui/job.py:409
msgid "Failed to update rules from: {}"
msgstr ""

#: tblock_gui/job.py:417
msgid "Removing allowed matches"
msgstr ""

#: tblock_gui/job.py:419
msgid "Failed to apply allowing rules."
msgstr ""

#: tblock_gui/main.py:86
msgid "Enable"
msgstr "Slå på"

#: tblock_gui/main.py:87
msgid "Protection is inactive"
msgstr "Skydd är inaktivt"

#: tblock_gui/main.py:88
msgid "Enable protection to show statistics"
msgstr "Slå på skydd för att visa statistik"

#: tblock_gui/main.py:91
msgid "Disable"
msgstr "Slå av"

#: tblock_gui/main.py:92
msgid "Protection is active"
msgstr "Skydd är aktivt"

#: tblock_gui/main.py:94
#, python-brace-format
msgid "{0} filter list(s) enabled and {1} active rule(s)"
msgstr ""

#: tblock_gui/main.py:101
msgid "Repair"
msgstr ""

#: tblock_gui/main.py:102
msgid "Threat detected"
msgstr ""

#: tblock_gui/main.py:103
msgid "Your secure network may not be safe anymore"
msgstr ""

#: tblock_gui/main.py:115
msgid "An update is available!"
msgstr ""

#: tblock_gui/main.py:119 data/ui/tblock.ui:140
msgid "No update available"
msgstr ""

#: tblock_gui/main.py:137
msgid "Failed to enable protection and to build new hosts file."
msgstr ""

#: tblock_gui/main.py:143
msgid "Failed to disable protection."
msgstr ""

#: tblock_gui/rule_add.py:106
msgid "Please specify a valid domain"
msgstr ""

#: tblock_gui/rule_add.py:115
msgid "Failed to allow domain: {}"
msgstr ""

#: tblock_gui/rule_add.py:125
msgid "Failed to block domain: {}"
msgstr ""

#: tblock_gui/rule_add.py:143
msgid "Failed to redirect domain: {}"
msgstr ""

#: tblock_gui/rule_add.py:149
msgid "Please specify a valid IP address."
msgstr ""

#: tblock_gui/rules.py:110
msgid "Domain"
msgstr ""

#: tblock_gui/rules.py:113
msgid "Policy"
msgstr ""

#: tblock_gui/rules.py:116
msgid "IP address"
msgstr ""

#: tblock_gui/rules.py:182
msgid "Failed to delete rules"
msgstr ""

#: tblock_gui/settings.py:82
#, fuzzy
msgid "enabled"
msgstr "Slå på"

#: tblock_gui/settings.py:84
#, fuzzy
msgid "disabled"
msgstr "Slå av"

#: tblock_gui/settings.py:88
msgid "no database"
msgstr ""

#: tblock_gui/settings.py:92
msgid "locked"
msgstr ""

#: tblock_gui/settings.py:94
msgid "unlocked"
msgstr ""

#: tblock_gui/settings.py:153
msgid "Failed to unlock database."
msgstr ""

#: tblock_gui/warning.py:64
msgid ""
"Unlocking the database can cause errors if another instance of TBlock is "
"currently using the database."
msgstr ""

#: data/ui/tblock.ui:29
msgid "An Error Occurred"
msgstr ""

#: data/ui/tblock.ui:43
msgid "OK"
msgstr ""

#: data/ui/tblock.ui:81 data/ui/tblock.ui:1364
msgid "About TBlock"
msgstr ""

#: data/ui/tblock.ui:90
msgid "Restart wizard"
msgstr ""

#: data/ui/tblock.ui:99 data/ui/tblock.ui:2741
msgid "Settings"
msgstr ""

#: data/ui/tblock.ui:114 data/ui/tblock.ui:1307
msgid "Quit"
msgstr ""

#: data/ui/tblock.ui:150
msgid "More information here"
msgstr ""

#: data/ui/tblock.ui:168 data/tblock-gui.desktop:2
msgid "TBlock"
msgstr ""

#: data/ui/tblock.ui:331 data/ui/tblock.ui:2339
msgid "Edit rules"
msgstr ""

#: data/ui/tblock.ui:347 data/ui/tblock.ui:426
msgid "Manage filter lists"
msgstr ""

#: data/ui/tblock.ui:363
msgid "Refresh"
msgstr ""

#: data/ui/tblock.ui:389
msgid "About TBlock GUI"
msgstr ""

#: data/ui/tblock.ui:396
msgid "An anti-capitalist ad blocker that uses the hosts file"
msgstr ""

#: data/ui/tblock.ui:515
msgid "Homepage"
msgstr ""

#: data/ui/tblock.ui:532
msgid "Show more"
msgstr ""

#: data/ui/tblock.ui:620
msgid "Add custom"
msgstr ""

#: data/ui/tblock.ui:639
msgid "Sync index"
msgstr ""

#: data/ui/tblock.ui:658
msgid "Update all"
msgstr ""

#: data/ui/tblock.ui:677
msgid "Confirm"
msgstr ""

#: data/ui/tblock.ui:693 data/ui/tblock.ui:789 data/ui/tblock.ui:2415
#: data/ui/tblock.ui:2504 data/ui/tblock.ui:2763
msgid "Close"
msgstr ""

#: data/ui/tblock.ui:732
msgid "Operation is running"
msgstr ""

#: data/ui/tblock.ui:755
msgid ""
"Please be patient. It is important that you do not close this window until "
"the task is finished."
msgstr ""

#: data/ui/tblock.ui:773 data/ui/tblock.ui:2488
msgid "Go back"
msgstr ""

#: data/ui/tblock.ui:856
msgid "Add custom filter list"
msgstr ""

#: data/ui/tblock.ui:884
msgid "Enter the URL of the custom filter list you want to add:"
msgstr ""

#: data/ui/tblock.ui:912
msgid "Or select a local file:"
msgstr ""

#: data/ui/tblock.ui:950 data/ui/tblock.ui:1064 data/ui/tblock.ui:1404
#: data/ui/tblock.ui:1518 data/ui/tblock.ui:1633 data/ui/tblock.ui:1891
#: data/ui/tblock.ui:2155 data/ui/tblock.ui:2560
msgid "Cancel"
msgstr ""

#: data/ui/tblock.ui:967 data/ui/tblock.ui:1099 data/ui/tblock.ui:1323
#: data/ui/tblock.ui:1436 data/ui/tblock.ui:1550 data/ui/tblock.ui:1665
#: data/ui/tblock.ui:1923 data/ui/tblock.ui:2187
msgid "Next"
msgstr ""

#: data/ui/tblock.ui:1012
msgid "Enter the ID to give to your custom filter list:"
msgstr ""

#: data/ui/tblock.ui:1043
msgid "Specify the format of your custom filter list:"
msgstr ""

#: data/ui/tblock.ui:1081 data/ui/tblock.ui:1420 data/ui/tblock.ui:1534
#: data/ui/tblock.ui:1649 data/ui/tblock.ui:1907 data/ui/tblock.ui:2171
msgid "Back"
msgstr ""

#: data/ui/tblock.ui:1213 data/ui/tblock.ui:2271
msgid "Finish"
msgstr ""

#: data/ui/tblock.ui:1248
msgid "Initial configuration"
msgstr ""

#: data/ui/tblock.ui:1287
msgid "Welcome to TBlock!"
msgstr ""

#: data/ui/tblock.ui:1385
msgid ""
"TBlock is an open source content blocker. It can block ads and trackers, as "
"well as harmful websites. The blocking is system-wide, which means that your "
"whole system is protected. TBlock won't be able to block specific elements "
"on a page. It is no replacement for an antivirus software. However, it will "
"be able to block ads and trackers in all the apps that you use on your "
"machine."
msgstr ""

#: data/ui/tblock.ui:1478
msgid "Let's get started"
msgstr ""

#: data/ui/tblock.ui:1499
msgid ""
"Now, let's take a few minutes to configure TBlock for the first time. Since "
"we need to download the list of websites to block, please ensure that you "
"have a working internet connection. When you are ready, click Next to launch "
"the automatic setup."
msgstr ""

#: data/ui/tblock.ui:1592
msgid "Ads and trackers settings"
msgstr ""

#: data/ui/tblock.ui:1615
msgid "Select the profile that suits your needs the best:"
msgstr ""

#: data/ui/tblock.ui:1699
msgid "None"
msgstr ""

#: data/ui/tblock.ui:1715
msgid "Light"
msgstr ""

#: data/ui/tblock.ui:1731
msgid "Balanced"
msgstr ""

#: data/ui/tblock.ui:1747
msgid "Aggressive"
msgstr ""

#: data/ui/tblock.ui:1803
msgid "Security settings"
msgstr ""

#: data/ui/tblock.ui:1846
msgid "Block malicious websites"
msgstr ""

#: data/ui/tblock.ui:1866
msgid ""
"This includes domains that serve malware, as well as websites associated "
"with spam, scam, phishing or ransomware activity."
msgstr ""

#: data/ui/tblock.ui:1966
msgid "Safety and wellbeing settings"
msgstr ""

#: data/ui/tblock.ui:2012
msgid "Block pornographic websites"
msgstr ""

#: data/ui/tblock.ui:2035
msgid "Block websites that spread fake news"
msgstr ""

#: data/ui/tblock.ui:2058
msgid "Block websites related to addictive substances"
msgstr ""

#: data/ui/tblock.ui:2081
msgid "Block gambling websites"
msgstr ""

#: data/ui/tblock.ui:2104
msgid "Block illegal websites (piracy)"
msgstr ""

#: data/ui/tblock.ui:2127
msgid "Block hate speech and far right content"
msgstr ""

#: data/ui/tblock.ui:2230
msgid "Everything is being set up"
msgstr ""

#: data/ui/tblock.ui:2253
msgid ""
"This may take a few minutes, depending on the components you decided to "
"block. Do not close this window until the task is finished."
msgstr ""

#: data/ui/tblock.ui:2381
msgid "New"
msgstr ""

#: data/ui/tblock.ui:2399
msgid "Delete"
msgstr ""

#: data/ui/tblock.ui:2538
msgid "Add rule"
msgstr ""

#: data/ui/tblock.ui:2575
msgid "Save"
msgstr ""

#: data/ui/tblock.ui:2601
msgid "Enter domain:"
msgstr ""

#: data/ui/tblock.ui:2634
msgid "Redirect"
msgstr ""

#: data/ui/tblock.ui:2651
msgid "Allow"
msgstr ""

#: data/ui/tblock.ui:2668
msgid "Block"
msgstr ""

#: data/ui/tblock.ui:2708
msgid "IP address:"
msgstr ""

#: data/ui/tblock.ui:2815
msgid "Updates"
msgstr ""

#: data/ui/tblock.ui:2853
msgid "Check for updates at startup"
msgstr ""

#: data/ui/tblock.ui:2876
msgid "Use beta channel for updates"
msgstr ""

#: data/ui/tblock.ui:2899
msgid "Logging"
msgstr ""

#: data/ui/tblock.ui:2936
msgid "Enable GUI logs"
msgstr ""

#: data/ui/tblock.ui:2959
msgid "Troubleshooting"
msgstr ""

#: data/ui/tblock.ui:2981
msgid "Unlock database"
msgstr ""

#: data/ui/tblock.ui:3010
msgid "Status information"
msgstr ""

#: data/ui/tblock.ui:3036
msgid "Database status"
msgstr ""

#: data/ui/tblock.ui:3059
msgid "Storage used"
msgstr ""

#: data/ui/tblock.ui:3093
msgid "Automatic updates"
msgstr ""

#: data/ui/tblock.ui:3109
msgid "How to enable automatic updates?"
msgstr ""

#: data/ui/tblock.ui:3154
msgid "Warning"
msgstr ""

#: data/me.tblock.gui.policy:9
msgid "TBlock needs to modify the system to perform this operation"
msgstr ""

#: data/me.tblock.gui.policy:10
msgid "Authentication is required to use TBlock"
msgstr ""

#: data/tblock-gui.desktop:3
msgid "Systemwide ads/trackers blocker"
msgstr ""

#~ msgid ""
#~ "Warning: you should only grant the redirecting permission to filter lists "
#~ "that you trust. Otherwise, it may compromise your secure network."
#~ msgstr ""
#~ "Varning: du borde endast bevilja omdirigeringsprivilegier till "
#~ "filterlistor som du litar på. Annars kan det kompromettera ditt säkra "
#~ "nätverk."

#~ msgid "Description"
#~ msgstr "Beskrivning"
