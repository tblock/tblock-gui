# -*- coding: utf-8 -*-
#   _____ ____  _            _
#  |_   _| __ )| | ___   ___| | __
#    | | |  _ \| |/ _ \ / __| |/ /
#    | | | |_) | | (_) | (__|   <
#    |_| |____/|_|\___/ \___|_|\_\
#
# An anti-capitalist ad-blocker that uses the hosts file
# Copyright (C) 2021-2023 Twann <tw4nn@disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
import tblock
import tblock.utils
from tblock.config import create_dirs
from tblock.compat import init_db
import os
import shutil
import tblock_gui.const
from tblock_gui.backend import parse_args

EXECUTABLE_NAME = "test"

# Change PATH variables
__root__ = os.path.join(os.path.dirname(__file__), "fake_root")
__prefix__ = os.path.join(__root__, "usr", "lib")
tblock.config.Path.PREFIX = __prefix__
tblock.config.Path.CACHE = os.path.join(__root__, "var", "cache")
tblock.config.Path.CONFIG = os.path.join(__root__, "etc", "tblock.conf")
tblock.config.Path.DAEMON_PID = os.path.join(__root__, "run", "tblock.pid")
tblock.config.Path.RULES_DATABASE = os.path.join(__prefix__, "user.db")
tblock.config.Path.DATABASE = os.path.join(__prefix__, "storage.sqlite")
tblock.config.Path.DB_LOCK = os.path.join(__prefix__, ".db_lock")
tblock.config.Path.HOSTS = os.path.join(__root__, "etc", "hosts")
tblock.config.Path.HOSTS_BACKUP = os.path.join(__prefix__, "hosts.bak")
tblock.config.Path.BUILT_HOSTS_BACKUP = os.path.join(__prefix__, "active.hosts.bak")
tblock.config.Path.LOGS = os.path.join(__root__, "var", "log", "tblock.log")
tblock.config.Path.TMP_DIR = os.path.join(__root__, "tmp", "tblock")
tblock_gui.const.PATH_GUI.reload(__root__)


def _create_env():
    # Remove data from previous tests
    if os.path.isdir(__root__):
        shutil.rmtree(__root__)
    # Setup new test environment
    os.mkdir(__root__)
    create_dirs()
    os.mkdir(os.path.join(__root__, "etc"))
    init_db(True)
    with open(tblock.config.Path.HOSTS, "wt") as h:
        h.close()


class TestParseArgs(unittest.TestCase):
    def setUp(self):
        _create_env()
        self.rule = tblock.rules.Rule("test.example")

    def test_test_gui(self):
        try:
            parse_args([EXECUTABLE_NAME, "test-gui"])
        except SystemExit as exitcode:
            assert exitcode.code == 0

    def test_unlock(self):
        tblock.utils.lock_db()
        assert tblock.utils.db_is_locked()
        parse_args([EXECUTABLE_NAME, "unlock"])
        assert not tblock.utils.db_is_locked()

    def test_build(self):
        assert tblock.hosts.hosts_are_default()
        parse_args([EXECUTABLE_NAME, "build"])
        assert not tblock.hosts.hosts_are_default()

    def test_enable(self):
        assert tblock.hosts.hosts_are_default()
        tblock.hosts.update_hosts(quiet=True, do_not_prompt=True)
        assert not tblock.hosts.hosts_are_default()
        tblock.hosts.restore_hosts(quiet=True, do_not_prompt=True)
        assert tblock.hosts.hosts_are_default()
        parse_args([EXECUTABLE_NAME, "enable"])
        assert not tblock.hosts.hosts_are_default()

    def test_disable(self):
        assert tblock.hosts.hosts_are_default()
        tblock.hosts.update_hosts(quiet=True, do_not_prompt=True)
        assert not tblock.hosts.hosts_are_default()
        parse_args([EXECUTABLE_NAME, "disable"])
        assert tblock.hosts.hosts_are_default()

    def test_allow(self):
        assert not self.rule.exists
        parse_args([EXECUTABLE_NAME, "allow", self.rule.domain])
        self.rule = tblock.rules.Rule(self.rule.domain)
        assert self.rule.exists and self.rule.policy == tblock.ALLOW

    def test_block(self):
        assert not self.rule.exists
        parse_args([EXECUTABLE_NAME, "block", self.rule.domain])
        self.rule = tblock.rules.Rule(self.rule.domain)
        assert self.rule.exists and self.rule.policy == tblock.BLOCK

    def test_redirect(self):
        assert not self.rule.exists
        parse_args([EXECUTABLE_NAME, "redirect", self.rule.domain, "127.0.0.1"])
        self.rule = tblock.rules.Rule(self.rule.domain)
        assert self.rule.exists and self.rule.policy == tblock.REDIRECT

    def test_delete(self):
        assert not self.rule.exists
        parse_args([EXECUTABLE_NAME, "block", self.rule.domain])
        self.rule = tblock.rules.Rule(self.rule.domain)
        assert self.rule.exists and self.rule.policy == tblock.BLOCK
        parse_args([EXECUTABLE_NAME, "delete", self.rule.domain])
        self.rule = tblock.rules.Rule(self.rule.domain)
        assert not self.rule.exists


class TestParseArgsWithNetwork(unittest.TestCase):
    def setUp(self):
        self.list_ = tblock.filters.Filter("tblock-experimental")

    def test_00_sync(self):
        _create_env()
        assert tblock.filters.get_current_repo_index_version() == 0
        parse_args([EXECUTABLE_NAME, "sync"])
        assert tblock.filters.get_current_repo_index_version() != 0

    def test_01_download(self):
        tblock.filters.sync_filter_list_repo(quiet=True)
        assert not os.path.isfile(self.list_.tmp_file)
        parse_args([EXECUTABLE_NAME, "download", self.list_.id])
        assert os.path.isfile(self.list_.tmp_file)

    def test_02_subscribe(self):
        assert not self.list_.subscribing
        parse_args([EXECUTABLE_NAME, "subscribe", self.list_.id])
        self.list_ = tblock.filters.Filter("tblock-experimental")
        assert self.list_.subscribing

    def test_03_update(self):
        assert self.list_.get_rules_count() == 0
        parse_args([EXECUTABLE_NAME, "update", self.list_.id])
        self.list_ = tblock.filters.Filter("tblock-experimental")
        assert self.list_.get_rules_count() != 0

    def test_04_rm_cache(self):
        assert self.list_.cache_exists()
        parse_args([EXECUTABLE_NAME, "rm-cache", self.list_.id])
        assert not self.list_.cache_exists()

    def test_05_remove(self):
        assert self.list_.subscribing
        parse_args([EXECUTABLE_NAME, "remove", self.list_.id])
        self.list_ = tblock.filters.Filter("tblock-experimental")
        assert not self.list_.subscribing

    def test_10_icon(self):
        assert not os.path.isfile(os.path.join(tblock.Path.TMP_DIR, "icons.tar.xz"))
        parse_args([EXECUTABLE_NAME, "icon"])
        assert os.path.isfile(os.path.join(tblock.Path.TMP_DIR, "icons.tar.xz"))

    def test_11_extract(self):
        parse_args([EXECUTABLE_NAME, "extract"])
