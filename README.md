<p align="center">
<img align="center" src="https://codeberg.org/tblock/tblock-gui/raw/commit/2456e9651103baef81f3a6c25cce97bf9d0252ee/assets/banner.png">
</p>
<p align="center">
<h1 align="center">TBlock GUI</h1>
</p>
<p align="center">
<a href="https://translate.codeberg.org/engage/tblock/"><img src="https://translate.codeberg.org/widgets/tblock/-/tblock-gui/svg-badge.svg" alt="Translation status" /></a>
<a href="https://ci.codeberg.org/tblock/tblock-gui"><img src="https://ci.codeberg.org/api/badges/tblock/tblock-gui/status.svg" alt="status-badge" /></a>
<a href="https://github.com/humanetech-community/awesome-humane-tech"><img src="https://codeberg.org/tblock/tblock/raw/branch/main/assets/icons/humane-tech-badge.svg" alt="Awesome Humane Tech"></a>
<a href="https://stopthemingmy.app/"><img src="https://codeberg.org/tblock/tblock-gui/raw/branch/main/assets/pleasedonttheme.svg" alt="Please don't theme our apps"></a>
<a href="https://nogithub.codeberg.page"><img src="https://nogithub.codeberg.page/badge.svg" alt="Please don't upload to GitHub"></a>
</p>

## Description

TBlock GUI is the official graphical user interface of the TBlock content blocker.

## Installation

| OS | Repository/Package |
| --- | --- |
| Arch Linux (AUR) | [tblock-gui](https://aur.archlinux.org/packages/tblock-gui) |
| Fedora (COPR) | [twann/tblock-gui](https://copr.fedorainfracloud.org/coprs/twann/tblock/package/tblock-gui/) |
| Debian | [tblock-gui](https://codeberg.org/tblock/tblock-gui/releases) |
| Ubuntu (PPA) | [twann4/tblock-gui](https://launchpad.net/~twann4/+archive/ubuntu/tblock) |

Before installing manually, please ensure that the following packages are installed on your machine:

- `python3`
- `python3-gobject`
- `gtk3`
- `polkit`
- `hicolor-icon-theme`
- `glib2`
- `tblock` (version 2.6.0 or later)

```sh
$ git clone https://codeberg.org/tblock/tblock-gui.git
$ cd tblock-gui/
$ make
$ sudo make install
```

## Usage

You can launch TBlock GUI using:

1. The desktop file
2. The command-line, by executing `tblockg`

## Roadmap

- [x] Add settings window
- [ ] Port to GTK4
- [ ] Show more info about filter lists (#10)
- [ ] Use Gtk.Template instead of Gtk.Builder\*
- [ ] Add a GUI for the converter (#11)
- [ ] Add a status tray icon (#14)
- [ ] Enable daemon at installation time (tblock/tblock#65)

\* Gtk.Template is currently not compatible with i18n, so we'll have to find a workaround.

## Screenshots

| ![](https://codeberg.org/tblock/tblock-gui/raw/commit/229ec9c1124440f929c083c6674083189bfff949/assets/screenshot_1.png) | ![](https://codeberg.org/tblock/tblock-gui/raw/commit/229ec9c1124440f929c083c6674083189bfff949/assets/screenshot_2.png)
| --- | --- | 
| ![](https://codeberg.org/tblock/tblock-gui/raw/commit/229ec9c1124440f929c083c6674083189bfff949/assets/screenshot_3.png) | ![](https://codeberg.org/tblock/tblock-gui/raw/commit/229ec9c1124440f929c083c6674083189bfff949/assets/screenshot_4.png)

## Libraries

Here is a list of all libraries used in the project:

| Name | Author | License |
| --- | --- | --- |
| [pygobject](https://pygobject.readthedocs.io/) | James Henstridge | LGPLv2+ |
| [tblock](https://tblock.codeberg.page/) | Twann | GPLv3 |
| [requests](https://requests.readthedocs.io/) | Kenneth Reitz | Apache 2.0 |

## License

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](https://www.gnu.org/licenses/gpl-3.0)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
