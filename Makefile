#!/usr/bin/env make -j2

# TBlock default Makefile
# Run 'make help' to show help message

# Define variables
PYTHON ?= /usr/bin/env python3
ROOT ?= /
DESTDIR ?= $(ROOT)/usr

all: build

.PHONY: all clean build-pypi install install-python-module install-files uninstall

clean:
	rm -rf translate build dist *.egg-info tblock_gui/__pycache__

build: translate
	$(PYTHON) setup.py build

build-pypi:
	$(PYTHON) -m build

xgettext:
	xgettext -k_ -kN_ -L Python tblock_gui/*.py -o po/tblock-gui.pot
	xgettext -k_ -kN_ -L Glade data/ui/tblock.ui -o po/tblock-gui.pot -j
	xgettext data/me.tblock.gui.policy -o po/tblock-gui.pot -j
	xgettext data/tblock-gui.desktop -o po/tblock-gui.pot -j
	for x in po/*.po; do \
		msgmerge --update $$x po/tblock-gui.pot; \
	done

translate:
	mkdir -p translate
	for x in po/*.po; do \
		msgfmt $$x -o "translate/"$$(basename $${x%.*}.mo); \
	done

test: test-pytest test-flake8 test-lint test-bandit

test-pytest:
	pytest -v

test-flake8:
	flake8 .

test-lint:
	pylint tblock_gui

test-bandit:
	bandit -rv tblock_gui --ini setup.cfg

install: install-python-module install-files install-translations

install-python-module: 
	$(PYTHON) setup.py install --skip-build --optimize=1 --root=$(ROOT)

install-files: install-icons
	install -Dm644 data/me.tblock.gui.policy $(DESTDIR)/share/polkit-1/actions/me.tblock.gui.policy
	install -Dm755 data/tblock-gui.desktop $(DESTDIR)/share/applications/tblock-gui.desktop
	install -Dm755 data/bin/tblockg $(DESTDIR)/bin/tblockg
	install -Dm755 data/bin/gui_helper $(DESTDIR)/lib/tblock/gui_helper
	install -Dm644 data/ui/banner.png $(DESTDIR)/share/tblock-gui/banner.png
	install -Dm644 data/ui/tblock-status-active.png $(DESTDIR)/share/tblock-gui/tblock-status-active.png
	install -Dm644 data/ui/tblock-status-disabled.png $(DESTDIR)/share/tblock-gui/tblock-status-disabled.png
	install -Dm644 data/ui/tblock-status-warning.png $(DESTDIR)/share/tblock-gui/tblock-status-warning.png
	install -Dm644 data/ui/tblock.png $(DESTDIR)/share/tblock-gui/tblock.png
	install -Dm644 data/ui/unknown.png $(DESTDIR)/share/tblock-gui/unknown.png
	install -Dm644 data/ui/tblock.ui $(DESTDIR)/share/tblock-gui/tblock.ui
	install -Dm644 TRANSLATORS.txt $(DESTDIR)/share/tblock-gui/TRANSLATORS.txt

install-icons:
	install -Dm644 data/icons/tblock1024.png $(DESTDIR)/share/icons/hicolor/1024x1024/apps/tblock.png
	install -Dm644 data/icons/tblock512.png $(DESTDIR)/share/icons/hicolor/512x512/apps/tblock.png
	install -Dm644 data/icons/tblock384.png $(DESTDIR)/share/icons/hicolor/384x384/apps/tblock.png
	install -Dm644 data/icons/tblock256.png $(DESTDIR)/share/icons/hicolor/256x256/apps/tblock.png
	install -Dm644 data/icons/tblock192.png $(DESTDIR)/share/icons/hicolor/192x192/apps/tblock.png
	install -Dm644 data/icons/tblock150.png $(DESTDIR)/share/icons/hicolor/150x150/apps/tblock.png
	install -Dm644 data/icons/tblock128.png $(DESTDIR)/share/icons/hicolor/128x128/apps/tblock.png
	install -Dm644 data/icons/tblock96.png $(DESTDIR)/share/icons/hicolor/96x96/apps/tblock.png
	install -Dm644 data/icons/tblock72.png $(DESTDIR)/share/icons/hicolor/72x72/apps/tblock.png
	install -Dm644 data/icons/tblock64.png $(DESTDIR)/share/icons/hicolor/64x64/apps/tblock.png
	install -Dm644 data/icons/tblock48.png $(DESTDIR)/share/icons/hicolor/48x48/apps/tblock.png
	install -Dm644 data/icons/tblock32.png $(DESTDIR)/share/icons/hicolor/32x32/apps/tblock.png
	install -Dm644 data/icons/tblock24.png $(DESTDIR)/share/icons/hicolor/24x24/apps/tblock.png
	install -Dm644 data/icons/tblock22.png $(DESTDIR)/share/icons/hicolor/22x22/apps/tblock.png
	install -Dm644 data/icons/tblock16.png $(DESTDIR)/share/icons/hicolor/16x16/apps/tblock.png
	install -Dm644 data/icons/tblock.svg $(DESTDIR)/share/icons/hicolor/scalable/apps/tblock.svg

install-translations:
	for x in translate/*.mo; do \
		install -Dm644 $$x "$(DESTDIR)/share/locale/"$$(basename $${x%.mo})"/LC_MESSAGES/tblock-gui.mo"; \
	done

uninstall:
	rm -f $(DESTDIR)/bin/tblockg
	rm -f $(DESTDIR)/lib/tblock/gui_helper
	rm -rf $(DESTDIR)/lib/python3.*/site-packages/tblock_gui*
	rm -rf $(DESTDIR)/share/tblock-gui/
	rm -f $(DESTDIR)/share/icons/hicolor/1024x1024/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/512x512/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/384x384/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/256x256/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/192x192/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/150x150/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/128x128/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/96x96/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/72x72/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/64x64/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/48x48/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/32x32/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/24x24/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/22x22/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/16x16/apps/tblock.png
	rm -f $(DESTDIR)/share/icons/hicolor/scalable/apps/tblock.svg
	rm -f $(DESTDIR)/share/polkit-1/actions/me.tblock.gui.policy
	rm -f $(DESTDIR)/share/applications/tblock-gui.desktop
	rm -f $(DESTDIR)/share/locale/*/LC_MESSAGES/tblock-gui.mo

