# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Standard libraries
import subprocess
from typing import List

# Local libraries
from .const import PATH_GUI


def pk_dummy_authenticate() -> int:
    """
    Function that triggers a polkit authentication in order to check if root access is available.
    :return:
    """
    return pk_subprocess_run(["test-gui"])


def pk_subprocess_run(arguments: List) -> int:
    """
    Function that parses arguments to call a subprocess
    :return:
    """
    if (
        "&" not in arguments
        and "&&" not in arguments
        and "|" not in arguments
        and "||" not in arguments
    ):
        process = subprocess.run(
            PATH_GUI.HELPER_EXECUTABLE + arguments,
            stdout=subprocess.PIPE,
            check=False,
            shell=False,
        )
        return process.returncode
    raise IOError("shell injection attempt was blocked")
