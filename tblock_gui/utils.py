# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Standard libraries
import math
import datetime
from typing import AnyStr

# Local libraries
from .const import PATH_GUI


def get_human_readable(number: int) -> AnyStr:
    """
    Convert a number into a human-readable string
    :param number: The integer to convert
    :return: AnyStr
    """
    units = ["", "K", "M"]
    num_size = len(str(number))
    if num_size >= 7:
        return f"{math.floor(number / (math.pow(10, 6)))}{units[2]}"
    if num_size >= 4:
        return f"{math.floor(number / (math.pow(10, 3)))}{units[1]}"
    return f"{number}{units[0]}"


def write_logs(message: AnyStr, logging_enabled: bool = True) -> None:
    """
    Write a message to the log file
    :param message: The message to write
    :param logging_enabled: Whether to write or not
    """
    if logging_enabled:
        with open(PATH_GUI.LOG_FILE, "at", encoding="utf-8") as _fb:
            _fb.write(
                datetime.datetime.now().strftime("[%Y-%m-%d %H:%M:%S] ")
                + message
                + "\n"
            )
