# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Standard libraries
import os.path
import gettext
from typing import AnyStr

# External libraries
import tblock
from tblock.converter import ADBLOCKPLUS, DNSMASQ, HOSTS, LIST, TBLOCK, TBLOCK_LEGACY

gettext.textdomain("tblock-gui")
_ = gettext.gettext


policies = {"A": "ALLOW", "B": "BLOCK", "R": "REDIRECT"}

ALL_SYNTAX_READABLE = {
    None: _("Auto-detect"),
    ADBLOCKPLUS: _("ABP/AdGuard/uBO format"),
    DNSMASQ: _("dnsmasq configuration"),
    HOSTS: _("Hosts file format"),
    LIST: _("Plain text domain list"),
    TBLOCK: _("TBlock format v2"),
    TBLOCK_LEGACY: _("TBlock format v1 (legacy)"),
}

FILTER_LISTS_ICON_PACK_URL = "https://tblock.codeberg.page/repo_icons/icons.tar.xz"
UPDATE_URL = {
    "stable": "https://codeberg.org/api/v1/repos/tblock/tblock-gui/releases?pre-release=false&limit=1",
    "beta": "https://codeberg.org/api/v1/repos/tblock/tblock-gui/releases?pre-release=true&limit=1",
}


class BasePathGUI:
    """
    This class contains the hardcoded location of the files required by the program to work
    """

    # pylint: disable=too-few-public-methods,invalid-name,too-many-instance-attributes

    def __init__(self, fs_root: AnyStr = None) -> None:
        self.HELPER_EXECUTABLE = ["/usr/bin/pkexec", "/usr/lib/tblock/gui_helper"]

        # The script is running on POSIX
        if os.name == "posix" or fs_root is not None:
            fs_root = "/" if fs_root is None else fs_root
            self.PREFIX = os.path.join(fs_root, "usr", "share", "tblock-gui")
            self.REPO_ICONS = os.path.join(fs_root, "var", "lib", "tblock-gui", "icons")

        # The script is running on Windows
        elif os.name == "nt":
            self.PREFIX = os.path.join(
                os.path.expandvars("%ALLUSERSPROFILE%"), "TBlock", "GUI"
            )
            self.REPO_ICONS = os.path.join(
                os.path.expandvars("%ALLUSERSPROFILE%"), "TBlock", "GUI", "Icons"
            )

        # If the script is running on an unsupported platform, raise an error
        else:
            raise OSError(
                _("TBlock is currently not supported on your operating system")
            )

        # Define other paths
        self.ENABLED_IMAGE = os.path.join(self.PREFIX, "tblock-status-active.png")
        self.DISABLED_IMAGE = os.path.join(self.PREFIX, "tblock-status-disabled.png")
        self.THREAT_IMAGE = os.path.join(self.PREFIX, "tblock-status-warning.png")
        self.UNKNOWN_IMAGE = os.path.join(self.PREFIX, "unknown.png")
        self.GLADE_UI = os.path.join(self.PREFIX, "tblock.ui")
        self.TRANSLATORS_CREDITS = os.path.join(self.PREFIX, "TRANSLATORS.txt")
        self.UI_DIR = os.path.join(self.PREFIX, "ui")
        self.LOG_FILE = tblock.Path.LOGS.replace(".log", "-gui.log")

    def reload(self, fs_root: AnyStr = None) -> None:
        """
        Reload the function and change the filesystem root.

        :param fs_root: The new filesystem root
        """
        # pylint: disable=unnecessary-dunder-call
        self.__init__(fs_root)


PATH_GUI = BasePathGUI()


class ExitStatusCode:
    """
    This contains the different exit codes known by TBlock for polkit authentication
    """

    # pylint: disable=too-few-public-methods

    PERMISSION_ERROR = 126
    FILE_NOT_FOUND = 127
    GENERIC = 1
    NO_ERROR = 0
