# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Standard libraries
import gettext
import sys

# External libraries
import gi
from tblock import __version__ as tblock_version

# Local imports
from .const import PATH_GUI
from .config import get_setting, GUISettings
from .main import MainActivity
from .error import ErrorActivity
from .intro import IntroActivity
from .rule_add import RuleAddActivity
from .rules import RulesActivity
from .custom_add import CustomAddActivity
from .about import AboutActivity
from .filter_lists import FilterListsActivity
from .settings import SettingsActivity
from .warning import WarningActivity
from .version import __version__

gi.require_version("Gtk", "3.0")
# pylint: disable=wrong-import-position,wrong-import-order
from gi.repository import Gtk, GLib

gettext.textdomain("tblock-gui")
_ = gettext.gettext


def print_artwork() -> None:
    """
    Print the logo and useful information on the terminal
    """
    print(
        """ _____  ____   _               _
|_   _|| __ ) | |  ___    ___ | | __
  | |  |  _ \\ | | / _ \\  / __|| |/ /
  | |  | |_) || || (_) || (__ |   <
  |_|  |____/ |_| \\___/  \\___||_|\\_\\
"""
    )
    print(
        f"  Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>\n\n"
        f"  System information\n"
        f"  ------------------------------\n"
        f"  Python version:  {sys.version}\n"
        f"  GUI version:     {__version__}\n"
        f"  TBlock version:  {tblock_version}\n"
    )


class TBlockGUI(Gtk.Application):
    """
    Base application (TBlock GUI)
    """

    # pylint: disable=too-many-instance-attributes,too-few-public-methods,missing-function-docstring

    def __init__(self) -> None:
        super().__init__(application_id="me.tblock.gui")
        GLib.set_application_name("TBlock")

        self.builder = Gtk.Builder()
        self.builder.set_translation_domain("tblock-gui")
        self.builder.add_from_file(PATH_GUI.GLADE_UI)

        self.main_window = None
        self.intro_window = None
        self.filter_lists_window = None
        self.rule_add_window = None
        self.rules_window = None
        self.settings_window = None
        self.custom_add_window = None
        self.about_window = None
        self.error_window = None
        self.warning_window = None

    def do_activate(self, *_args, **_kwargs) -> None:
        print_artwork()
        configured = get_setting(GUISettings.SETUP_COMPLETED.value)
        logging_enabled = get_setting(GUISettings.LOGGING_ENABLED.value)
        check_update = get_setting(GUISettings.UPDATE_STARTUP_CHECK.value)
        update_beta = get_setting(GUISettings.UPDATE_BETA.value)
        print(
            f"  Internal settings\n"
            f"  ------------------------------\n"
            f"  gui.logging.enabled       = {int(logging_enabled)}\n"
            f"  gui.setup.completed       = {int(configured)}\n"
            f"  gui.update.beta           = {int(update_beta)}\n"
            f"  gui.update.startup_check  = {int(check_update)}\n"
        )
        self.main_window = self.main_window or MainActivity(application=self)
        self.intro_window = self.intro_window or IntroActivity(application=self)
        self.filter_lists_window = self.filter_lists_window or FilterListsActivity(
            application=self
        )
        self.rule_add_window = self.rule_add_window or RuleAddActivity(application=self)
        self.rules_window = self.rules_window or RulesActivity(application=self)
        self.settings_window = self.settings_window or SettingsActivity(
            application=self
        )
        self.custom_add_window = self.custom_add_window or CustomAddActivity(
            application=self
        )
        self.about_window = self.about_window or AboutActivity(application=self)
        self.error_window = self.error_window or ErrorActivity(application=self)
        self.warning_window = self.warning_window or WarningActivity(application=self)
        if configured is None or not bool(int(configured)):
            self.intro_window.start()
        else:
            self.main_window.start()
