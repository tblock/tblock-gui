# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Standard libraries
import gettext

gettext.textdomain("tblock-gui")
_ = gettext.gettext


class WarningActivity:
    """
    Class that interacts with the activity of the same name from the glade UI
    """

    def __init__(self, application) -> None:
        """
        Initiate the activity

        :param application: An object of type Gtk.Application
        """
        self._application = application
        self.window = self._application.builder.get_object("WarningActivity")

    def prompt(self, message: str) -> bool:
        """
        Run a confirmation dialog before executing an operation

        :param message: The message to show
        :return: bool
        """
        self.window.format_secondary_text(message)
        prompt = self.window.run()
        self.quit()
        return prompt == -5

    def prompt_database_unlock(self) -> bool:
        """
        Run a confirmation dialog before unlocking the database

        :return: bool
        """
        return self.prompt(
            _(
                "Unlocking the database can cause errors if another instance"
                " of TBlock is currently using the database."
            )
        )

    def quit(self) -> None:
        """
        Quit the activity
        """
        self.window.hide()
