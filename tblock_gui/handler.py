# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# External libraries
import tblock
from tblock.const import Profile, Components
import gi

# Local libraries
from .config import GUISettings

gi.require_version("Gtk", "3.0")
# pylint: disable=wrong-import-position,wrong-import-order
from gi.repository import GLib


class Handler:
    """
    The handler for all signals emitted from the glade UI
    """

    # pylint: disable=too-many-public-methods,too-many-instance-attributes

    def __init__(self, application) -> None:
        """
        Handler for the signals emitted from the Glade interface

        :param application: An object of type Gtk.Application
        """
        self._application = application

    # MainActivity

    def on_refresh_status(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.main_window.refresh()

    def on_destroy(self, *_args) -> True:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.main_window.quit()
        return True

    def on_toggle_status(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.main_window.run()

    # IntroActivity

    def on_intro(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.start()

    def on_intro_profile_none(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.select_profile(Profile.NONE)

    def on_intro_profile_light(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.select_profile(Profile.LIGHT)

    def on_intro_profile_balanced(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.select_profile(Profile.BALANCED)

    def on_intro_profile_aggressive(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.select_profile(Profile.AGGRESSIVE)

    def on_intro_security_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.toggle_settings(Components.SECURITY)

    def on_intro_fake_news_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.toggle_settings(Components.FAKE_NEWS)

    def on_intro_porn_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.toggle_settings(Components.PORNOGRAPHY)

    def on_intro_piracy_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.toggle_settings(Components.PIRACY)

    def on_intro_drugs_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.toggle_settings(Components.DRUGS)

    def on_intro_gambling_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.toggle_settings(Components.GAMBLING)

    def on_intro_hate_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.toggle_settings(Components.HATE)

    def on_intro_go_to_0(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.go_to_page(0)

    def on_intro_go_to_1(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.go_to_page(1)

    def on_intro_go_to_2(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.go_to_page(2)

    def on_intro_go_to_3(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.go_to_page(3)

    def on_intro_go_to_4(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.go_to_page(4)

    def on_intro_go_to_5(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.go_to_page(5)

    def on_intro_go_to_6(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.run_thread()

    def on_intro_destroy(self, *_args) -> True:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.intro_window.quit()
        return True

    def on_intro_terminate(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        GLib.timeout_add(400, self._application.main_window.start)
        self._application.intro_window.quit()

    # RuleAddActivity

    def on_rule_add(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rule_add_window.start()

    def on_rule_add_allow(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rule_add_window.select_policy(tblock.ALLOW)

    def on_rule_add_block(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rule_add_window.select_policy(tblock.BLOCK)

    def on_rule_add_redirect(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rule_add_window.select_policy(tblock.REDIRECT)

    def on_rule_add_destroy(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rule_add_window.quit()

    def on_rule_add_save(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rule_add_window.run()
        self._application.rules_window.refresh()

    # RulesActivity

    def on_rules(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rules_window.start()

    def on_rules_delete(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rules_window.run_thread()

    def on_rules_goback(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        # This will simply hide the delete button, refresh the rules and change the visible stack child
        self._application.rules_window.start()

    def on_rules_destroy(self, *_args) -> True:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.rules_window.quit()
        return True

    # CustomAddActivity

    def on_custom_add(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.custom_add_window.start()

    def on_custom_add_go_to_0(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.custom_add_window.go_to_page(0)

    def on_custom_add_go_to_1(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.custom_add_window.go_to_page(1)

    def on_custom_add_go_to_2(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.custom_add_window.run_thread()

    def on_custom_add_update_id(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.custom_add_window.edit_id_field()

    def on_custom_add_select_file(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.custom_add_window.select_file()

    def on_custom_add_destroy(self, *_args) -> True:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.custom_add_window.quit()
        return True

    def on_custom_add_terminate(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.filter_lists_window.start()
        self._application.main_window.refresh()
        self._application.custom_add_window.quit()

    # FilterListsActivity

    def on_filter_lists(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.filter_lists_window.start()

    def on_filter_lists_search(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.filter_lists_window.search()

    def on_filter_lists_apply(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.filter_lists_window.run_thread()

    def on_filter_lists_sync(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.filter_lists_window.run_thread_sync()

    def on_filter_lists_update(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.filter_lists_window.run_thread_update()

    def on_filter_lists_goback(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.main_window.refresh()
        # This will simply refresh the filter lists and change the visible stack child
        self._application.filter_lists_window.start()

    def on_filter_lists_destroy(self, *_args) -> True:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.filter_lists_window.quit()
        return True

    def on_filter_lists_terminate(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.main_window.refresh()
        self._application.filter_lists_window.quit()

    def on_filter_lists_show_more(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        # This is not yet implemented

    # TroubleshootingActivity

    def on_settings(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.settings_window.start()

    def on_settings_unlock(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.settings_window.run_unlock()

    def on_settings_logging_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.settings_window.setting_switch(
            GUISettings.LOGGING_ENABLED.value
        )

    def on_settings_check_update_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.settings_window.setting_switch(
            GUISettings.UPDATE_STARTUP_CHECK.value
        )

    def on_settings_update_beta_switch(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.settings_window.setting_switch(GUISettings.UPDATE_BETA.value)

    def on_settings_destroy(self, *_args) -> bool:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.settings_window.quit()
        return True

    # AboutActivity

    def on_about(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.about_window.start()

    # ErrorActivity

    def on_error_quit(self, *_args) -> None:
        """
        Handler for the matching signal

        :param _args:
        """
        self._application.error_window.quit()
