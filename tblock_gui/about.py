# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Local libraries
from .version import __version__
from .const import PATH_GUI


class AboutActivity:
    """
    Class that interacts with the activity of the same name from the glade UI
    """

    # pylint: disable=too-few-public-methods

    def __init__(self, application) -> None:
        """
        Initiate the activity

        :param application: An object of type Gtk.Application
        """
        self._application = application
        self.window = self._application.builder.get_object("AboutActivity")
        self.window.set_application(self._application)
        self.window.set_version(__version__)
        try:
            with open(PATH_GUI.TRANSLATORS_CREDITS, "rt", encoding="utf-8") as _fb:
                translator_credits = _fb.read()
        except (PermissionError, FileNotFoundError):
            translator_credits = ""
        self.window.set_translator_credits(translator_credits)

    def start(self) -> None:
        """
        Start the activity
        """
        response = self.window.run()
        if response == -4:
            self.window.hide()
