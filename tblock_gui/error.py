# -*- coding: utf-8 -*-
#  _____  ____   _               _
# |_   _|| __ ) | |  ___    ___ | | __
#   | |  |  _ \ | | / _ \  / __|| |/ /
#   | |  | |_) || || (_) || (__ |   <
#   |_|  |____/ |_| \___/  \___||_|\_\
#
# Copyright (C) 2021-2023 Twann <tw4nn at disroot.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Standard libraries
import gettext

# Local libraries
from .const import ExitStatusCode

gettext.textdomain("tblock-gui")
_ = gettext.gettext


class ErrorActivity:
    """
    Class that interacts with the activity of the same name from the glade UI
    """

    def __init__(self, application) -> None:
        """
        Initiate the activity

        :param application: An object of type Gtk.Application
        """
        self._application = application
        self.window = self._application.builder.get_object("ErrorActivity")

    def handle_error(self, err_code: int) -> None:
        """
        Handle errors when running polkit authentication
        :param err_code: The exit code received
        """
        if err_code == ExitStatusCode.PERMISSION_ERROR:
            self.raise_permission_error()
        elif err_code == ExitStatusCode.FILE_NOT_FOUND:
            self.raise_no_tblock_executable_error()
        else:
            self.raise_unknown_error(err_code)

    def raise_error(self, message) -> None:
        """
        Raise a generic error

        :param message: The message to display in the error dialog
        """
        self.window.format_secondary_text(message)
        self.window.run()
        self.window.hide()

    def raise_permission_error(self) -> None:
        """
        Raise a permission error
        """
        self.raise_error(_("This operation requires admin privileges."))

    def raise_no_tblock_executable_error(self) -> None:
        """
        Raise a permission error
        """
        self.raise_error(
            _("TBlock executable was not found.\nPlease install it and then retry.")
        )

    def raise_unknown_error(self, err_code: int = None) -> None:
        """
        Raise an unknown error with an exit code
        :param err_code: The exit code
        """
        self.raise_error(_("An unknown error occurred.\nCode: {}").format(err_code))

    def raise_feature_unavailable_error(self) -> None:
        """
        Raise an unknown error for a feature that is not yet implemented
        :return:
        """
        self.raise_error(_("This feature is not yet supported."))

    def raise_db_locked_error(self) -> None:
        """
        Raise a 'database is locked' error
        """
        self.raise_error(
            _(
                "The database is locked. Please try again later.\n"
                "If this error persists, you can unlock the database "
                "in the navigation menu under TBlock > Troubleshooting."
            )
        )

    def raise_invalid_source_error(self) -> None:
        """
        Raise an invalid source error
        """
        self.raise_error(_("Please provide a valid URL/local file."))

    def raise_invalid_source_owner_error(self) -> None:
        """
        Raise an invalid source error
        """
        self.raise_error(_("File must be owned by root."))

    def raise_invalid_id_error(self) -> None:
        """
        Raise an invalid ID error
        """
        self.raise_error(
            _("Please specify a title to give to your custom filter list.")
        )

    def raise_source_exists_error(self) -> None:
        """
        Raise a source exists error
        """
        self.raise_error(_("Source already exists."))

    def raise_db_unlocked_error(self) -> None:
        """
        Raise a 'database is unlocked' error
        """
        self.raise_error(_("The database is already unlocked."))

    def quit(self) -> None:
        """
        Quit the activity
        """
        self.window.hide()
