Name:           tblock-gui
Version:        1.3.4
Release:        1%{?dist}
Summary:        Official graphical user interface for the TBlock content blocker

License:        GPLv3
URL:            https://tblock.me
Source0:        https://codeberg.org/tblock/tblock-gui/archive/%{version}.tar.gz

BuildRequires:  make
BuildRequires:  python
BuildRequires:	python-setuptools
BuildRequires:  gettext
Requires:       python
Requires:       python-requests
Requires:       python-gobject
Requires:       tblock
Requires:       gtk3
Requires:       glib2
Requires:       polkit
Requires:       coreutils
Requires:       hicolor-icon-theme

%description
Official graphical user interface for the TBlock content blocker

%global debug_package %{nil}

%prep
%autosetup

%build
make

%install
rm -rf %{buildroot}
make install ROOT=%{buildroot}

%files
%license LICENSE
/usr/bin/tblockg
/usr/share/polkit-1/actions/me.tblock.gui.policy
/usr/lib/python3.*/site-packages/tblock_gui/
/usr/lib/python3.*/site-packages/tblock_gui-*-py3.*.egg-info/
/usr/lib/tblock/gui_helper
/usr/share/applications/tblock-gui.desktop
/usr/share/icons/hicolor/scalable/apps/tblock.svg
/usr/share/icons/hicolor/*/apps/tblock.png
/usr/share/locale/*/LC_MESSAGES/tblock-gui.mo
/usr/share/tblock-gui/*

%changelog
* Thu Jul 20 12:43:00 CEST 2023 Twann <tw4nn@disroot.org>
- Compatibility fixes
