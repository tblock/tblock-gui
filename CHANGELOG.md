# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.5-beta]

### Changed

- Updated translations

### Removed

- Removed filter list permission (this was unsupported and did nothing since a long time)

### Security

- Display an error if custom filter list is not owned by root

## [1.3.4]

### Fixed

- Fix version number

## [1.3.3]

### Changed

- Compatibility fixes with tblock 2.7.0

## [1.3.2]

### Added

- First stable release

### Changed

- Updated German translations
- Changed banner in initial setup

## [1.3.2-rc.1]

### Fixed

- Fix version number

## [1.3.1-beta]

### Changed

- Update app icon

### Fixed

- Frozen app when database is locked and thread is running

## [1.3.0-beta]

### Added

- Allow to choose which components to block in initial setup (#12)
- Add settings window
- Allow to log GUI activity
- Optionally check for updates on startup

### Changed

- Clean up code
- Use Gtk.Application
- Only show setup wizard when the application is started for the first time
- Allow to choose among several components to block in initial setup
- Remove troubleshooting window (in favor of settings window)
- Redesign filter lists dialog
- Replace the navigation bar with a hamburger menu
- Change the polkit policy id
- Change the name of the "daemon" to "automatic updates (tblock/tblock#64)

### Fixed

- Group windows together
- Unlock database when sync fails
- Fix issue with filter lists dialog that tells "Operation was successful" even though it wasn't (#13)
- Fix conflict between the setup wizard and the running daemon (#15)
- Prevent enable and disable running at the same time
- Do not load the module in setup.py

## Security

- Run GUI as normal user and then run a subprocess that calls pkexec (#17)

## [1.2.0-beta]

### Added

- Search bar in filter lists activity
- A sidebar showing details about the selected filter lists
- Icons support for filter lists details
- Add translations to desktop launcher and polkit policy config
- Complete German, Spanish and Portuguese translations
- Show warning in main activity when hosts file built by TBlock does not match the saved checksum

### Changed

- Refactor filter lists activity (#9)
- Upgrade `tblock` to version `2.6.0`
- Use `black` code style
- Use `pylint` as linter

### Fixed

- Stop thread when closing a window while a process is running (#7)
- Do not focus any filter list by default when opening filter lists activity
- Open the correct stack page when starting filter lists activity for the first time
- Do not load Gtk outside of the `run()` function
- Allow to clear storage when TBlock is inactive

## [1.1.0-alpha] - 2022-11-03

### Added

- French translation

### Changed

- MainActivity size
- MainActivity button height and width

### Fixed

- Support for other languages
- Undefined `_()` function call in some modules
- Text wrap in IntroActivity and CustomAddActivity

## [1.0.0-alpha] - 2022-10-28

### Changed

- Brand new codebase

## [0.0.1-alpha] - 2022-10-17

### Added

- Initial release

[Unreleased]: https://codeberg.org/tblock/tblock-gui/src/branch/main
[1.3.5-beta]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.3.5-beta
[1.3.4]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.3.4
[1.3.3]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.3.3
[1.3.2]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.3.2
[1.3.2-rc.1]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.3.2-rc.1
[1.3.1-beta]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.3.1-beta
[1.3.0-beta]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.3.0-beta
[1.2.0-beta]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.2.0-beta
[1.1.0-alpha]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.1.0-alpha
[1.0.0-alpha]: https://codeberg.org/tblock/tblock-gui/releases/tag/1.0.0-alpha
[0.0.1-alpha]: https://codeberg.org/tblock/tblock-gui/src/tag/0.0.1-alpha
